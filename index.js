barba.init({
    transitions: [{
        leave(data) {
            return gsap.to('ul.transition li', {
                duration: .5,
                scaleY: 1,
                transformOrigin: "bottom left",
                stagger: .2
            });
        },
        after(data) {
            return gsap.to('ul.transition li', {
                duration: .5,
                scaleY: 0,
                transformOrigin: "bottom left",
                stagger: .1,
                delay: .1
            });
        }
    }]
});